#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#


define Profile/SPROUTKIT
	NAME:=Sprout SPROUT-KIT
	PACKAGES:=\
		kmod-ledtrig-usbdev
endef

define Profile/SPROUTKIT/Description
	Package set for Sprout SPROUT-KIT board .
endef

$(eval $(call Profile,SPROUTKIT))

