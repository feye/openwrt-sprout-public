#!/usr/bin/python3
import re
import os
import urllib.request
import json
from docutils import core
from multiprocessing import Pool

MAKEFILE = """#
# Copyright (C) 2006 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:={wrtname}
PKG_VERSION:={version}
PKG_RELEASE:=1

PKG_SOURCE:={filename}
PKG_SOURCE_URL:={directory}
PKG_MD5SUM:={md5}
PKG_BUILD_DIR:=$(BUILD_DIR)/{name}-$(PKG_VERSION)/

PKG_BUILD_DEPENDS:=python3-setuptools

include $(INCLUDE_DIR)/package.mk
$(call include_mk, python3-package.mk)

define Package/{wrtname}
  SUBMENU:=Python
  SECTION:=lang
  CATEGORY:=Languages
  TITLE:={summary}
  URL:={home_page}
  DEPENDS:={wrtdeps}
endef

define Package/{wrtname}/description
{description}
endef

define Build/Compile
	$(call Build/Compile/PyMod,., \\
		install --prefix="/usr" --root="$(PKG_INSTALL_DIR)" \\
	)
endef

define Build/InstallDev
	$(INSTALL_DIR) $(STAGING_DIR)$(PYTHON_PKG_DIR)
	$(CP) \\
		$(PKG_INSTALL_DIR)$(PYTHON_PKG_DIR)/* \\
		$(STAGING_DIR)$(PYTHON_PKG_DIR)/
	[ ! -e $(PKG_INSTALL_DIR)/usr/include ] || $(CP) \\
		$(PKG_INSTALL_DIR)/usr/include/* \\
		$(STAGING_DIR)/usr/include/
endef

define Package/{wrtname}/install
	$(INSTALL_DIR) $(1)$(PYTHON_PKG_DIR)
	$(CP) \\
		$(PKG_INSTALL_DIR)$(PYTHON_PKG_DIR)/* \\
		$(1)$(PYTHON_PKG_DIR)
{extrainstall}
endef

$(eval $(call BuildPackage,{wrtname}))
"""

EXTRAINSTALL = """	$(INSTALL_DIR) $(1){dir}
	$(CP) \\
		$(PKG_INSTALL_DIR){filefrom} \\
		$(1){fileto}
	$(SED) \\
		's,^#!.*,#!/usr/bin/env python$(PYTHON_BINABI),g' \\
		$(1){fileto}

"""

class PyPItoMakefile:
    def __init__(self, args, run=True):
        self.pypiname, self.wrtname, self.wrtdeps, self.extrafiles = args
        self.maketemplate = MAKEFILE
        self.extratemplate = EXTRAINSTALL
        
        self.package = {}
        self.package["wrtname"] = self.wrtname
        self.package["wrtdeps"] = self.wrtdeps
        self.package["extrainstall"] = ""
        
        if run:
            self.getpypi()
            self.getdownload()
            self.gendesc()
            self.genextra()
            self.genmakefile()
    
    def getpypi(self):
        url = "http://pypi.python.org/pypi/%s/json" % self.pypiname
        f = urllib.request.urlopen(url)
        pypijson = f.read().decode("UTF-8")
        pypi = json.loads(pypijson)
        self.package["version"] = pypi["info"]["version"]
        self.package["name"] = pypi["info"]["name"]
        self.package["summary"] = pypi["info"]["summary"]
        self.package["home_page"] = pypi["info"]["home_page"]
        self.urls = pypi["urls"]
        self.restruct = pypi["info"]["description"]
    
    def getdownload(self):
        sdists = [url for url in self.urls if url["packagetype"] == "sdist"]
        sdists.sort(key=lambda url: "A" if ".tar." in url["filename"] else "B")
        self.package["directory"] = re.sub(r"[^/]*$", r"", sdists[0]["url"])
        self.package["filename"] = sdists[0]["filename"]
        self.package["md5"] = sdists[0]["md5_digest"]
    
    def gendesc(self):
        restruct = re.sub(r"contents::", r"", self.restruct)
        html = core.publish_string(source=restruct, writer_name='html')
        html = html.decode("UTF-8")
        html = html[html.find('<body>')+6:html.find('</body>')].strip()
        text = re.sub(r"<[^>]*>", r"", html)
        text = re.sub(r"[^A-Za-z0-9\-_.:/*\s]", r"", text)
        txts = text.split("\n")
        txts = [txt for txt in txts if not re.search(r"^\s*$",txt)]
        txts = [" " + txt for txt in txts]
        if len(txts) > 10:
            txts = txts[0:10]
        text = "\n".join(txts)
        self.package["description"] = text
    
    def writeifchanged(self, filename, text):
        try:
            f = open(filename, "r", encoding="UTF-8")
            oldtext = f.read()
            f.close()
        except:
            oldtext = ""
        if oldtext != text:
            f = open(filename, "w+", encoding="UTF-8")
            f.write(text)
            f.close()
    
    def genextra(self):
        for filefrom, fileto in self.extrafiles:
            extradir = os.path.dirname(fileto)
            x = self.extratemplate.format_map({"filefrom":filefrom, "fileto":fileto, "dir":extradir})
            self.package["extrainstall"] = self.package["extrainstall"] + x
    
    def genmakefile(self):
        os.makedirs(self.wrtname, exist_ok=True)
        makefile = os.path.join(self.wrtname, "Makefile")
        self.writeifchanged(makefile, self.maketemplate.format_map(self.package))

p = Pool(10)
p.map(PyPItoMakefile, [
    #Fails to compile lxml
    ("Cython",
     "cython3",
     "+python3",
     [("/usr/bin/cython", "/usr/bin/cython3")]),
    
    ("Django",
     "python3-django",
     "+python3",
     [("/usr/bin/django-admin.py", "/usr/bin/django-admin3")]),
    
    ("Flask",
     "python3-flask",
     "+python3 +python3-werkzeug +python3-jinja2",
     []),

    ("Jinja2",
     "python3-jinja2",
     "+python3",
     []),
    
    #Fails to load jpeg
    ("Pillow",
     "python3-pil",
     "+python3 +libjpeg +libfreetype",
     [ ("/usr/bin/pilprint.py", "/usr/bin/pilprint3"),
       ("/usr/bin/pilfont.py", "/usr/bin/pilfont3"),
       ("/usr/bin/pilfile.py", "/usr/bin/pilfile3"),
       ("/usr/bin/pildriver.py", "/usr/bin/pildriver3"),
       ("/usr/bin/pilconvert.py", "/usr/bin/pilconvert3"),]),
    
    ("PyMySQL",
     "python3-pymysql",
     "+python3",
     []),
    
    ("PyYAML",
     "python3-yaml",
     "+python3 +libyaml",
     []),
    
    ("SQLAlchemy",
     "python3-sqlalchemy",
     "+python3",
     []),
    
    #Error: illegal operands `addu'
    ("greenlet",
     "python3-greenlet",
     "+python3 @BROKEN",
     []),
    
    ("httplib2",
     "python3-httplib2",
     "+python3",
     []),
    
    ("ipython",
     "ipython3",
     "+python3",
     [ ("/usr/bin/pycolor3", "/usr/bin/pycolor3"),
       ("/usr/bin/irunner3", "/usr/bin/irunner3"),
       ("/usr/bin/ipython3", "/usr/bin/ipython3"),
       ("/usr/bin/iptest3", "/usr/bin/iptest3"),
       ("/usr/bin/iplogger3", "/usr/bin/iplogger3"),
       ("/usr/bin/ipengine3", "/usr/bin/ipengine3"),
       ("/usr/bin/ipcontroller3", "/usr/bin/ipcontroller3"),
       ("/usr/bin/ipcluster3", "/usr/bin/ipcluster3")]),
    
    ("pip",
     "python3-pip",
     "+python3 +python3-setuptools",
     [("/usr/bin/pip3", "/usr/bin/pip3")]),
    
    ("psycopg2",
     "python3-psycopg2",
     "+python3 +libpq",
     []),
    
    ("py-postgresql",
     "python3-postgresql",
     "+python3 +libpq",
     []),
    
    ("pyOpenSSL",
     "python3-openssl",
     "+python3 +libopenssl",
     []),
    
    ("pycurl",
     "python3-pycurl",
     "+python3 +libcurl",
     []),
    
    ("pymongo",
     "python3-pymongo",
     "+python3",
     []),
    
    ("pyserial",
     "python3-serial",
     "+python3",
     [("/usr/bin/miniterm.py", "/usr/bin/miniterm3")]),
    
    ("python-dateutil",
     "python3-dateutil",
     "+python3",
     []),
    
    ("pytz",
     "python3-tz",
     "+python3",
     []),
    
    ("requests",
     "python3-requests",
     "+python3 +python3-urllib3",
     []),
    
    ("simplejson",
     "python3-simplejson",
     "+python3",
     []),
    
    ("six",
     "python3-six",
     "+python3",
     []),
    
    ("urllib3",
     "python3-urllib3",
     "+python3",
     []),
    
    ("Werkzeug",
     "python3-werkzeug",
     "+python3",
     []),
    
    ("lxml",
     "python3-lxml",
     "+python3 +libxml2 +libxslt +libexslt +zlib",
     []),
    
    #Needs UCLIBC_HAS_FENV set in toolchain/uClibc/config-0.9.33.2/common
    #Segmentation fault after import of scimath
    ("numpy",
     "python3-numpy",
     "+python3 @BROKEN",
     []),
])

def gensetuptools():
    pkg = PyPItoMakefile( ("setuptools",
     "python3-setuptools",
     "+python3",
     [("/usr/bin/easy_install", "/usr/bin/easy_install3")]), run=False )
    pkg.maketemplate = MAKEFILE.replace("PKG_BUILD_DEPENDS:=python3-setuptools\n\n","")
    pkg.getpypi()
    pkg.getdownload()
    pkg.gendesc()
    pkg.genextra()
    pkg.genmakefile()
gensetuptools()

def genpycrypto():
    pkg = PyPItoMakefile(
    ("pycrypto",
     "python3-crypto",
     "+python3 +libgmp",
     []), run=False )
    pkg.getpypi()
    pkg.package["directory"] = "https://pypi.python.org/packages/source/p/pycrypto/"
    pkg.package["filename"] = "pycrypto-2.6.1.tar.gz"
    pkg.package["md5"] = "55a61a054aa66812daf5161a0d5d7eda"
    pkg.gendesc()
    pkg.genextra()
    pkg.genmakefile()
genpycrypto()
